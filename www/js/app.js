// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.factories','ngStorage','ngCordova'])

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.scrolling.jsScrolling(true);
})

.run(function($ionicPlatform,$rootScope,$http,DealsFactory) {
	$rootScope.Host = "http://www.tapper.co.il/needy/php/";
	$rootScope.Data = null;
	$rootScope.categories = [];
	$rootScope.suppliers = [];
	$rootScope.deals = [];
	$rootScope.USER = [];
	
	DealsFactory.getData().then(function(data)
	{
		DealsFactory.getDeals();
	});
	
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
	
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  
    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })
	 .state('app.deal', {
      url: '/deal/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/deal.html',
          controller: 'DealCtrl'
        }
      }
    })
	 .state('app.profile', {
      url: '/profile',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile.html',
          controller: 'ProfileCtrl'
        }
      }
    })
	 .state('app.editDeal', {
      url: '/editDeal/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/editDeal.html',
          controller: 'editDealCtrl'
        }
      }
    })
	.state('app.editProfile', {
      url: '/editProfile',
      views: {
        'menuContent': {
          templateUrl: 'templates/editProfile.html',
          controller: 'editProfileCtrl'
        }
      }
    })
	.state('app.dealGallery', {
      url: '/dealGallery',
      views: {
        'menuContent': {
          templateUrl: 'templates/dealGallery.html',
          controller: 'dealGalleryCtrl'
        }
      }
    })
	
	.state('app.register', {
      url: '/register',
      views: {
        'menuContent': {
          templateUrl: 'templates/register.html',
          controller: 'registerCtrl'
        }
      }
    })

	.state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'loginCtrl'
        }
      }
    })

	
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});
