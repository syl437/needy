angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,DealsFactory,$localStorage) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  SupplierIndex = 2;
  $scope.sideMenuWidth = window.innerWidth*0.8;
  
  DealsFactory.getCustomerById().then(function(data){
			console.log("getCustomerById")
  });
  
  $scope.logout = function()
  {
	  $localStorage.userid = '';
	  $localStorage.name = '';
	  $localStorage.email = '';
	  $localStorage.phone = '';
  }
})

.controller('MainCtrl', function($scope,DealsFactory,$rootScope,$localStorage) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		$scope.navTitle='<img class="title-image" src="img/head_logo.png" />'
		$scope.Deals = [];
		$scope.host = $rootScope.Host
		$scope.Deals = $rootScope.deals;
		$scope.userId = $localStorage.userid;
		console.log("Data2 : " , $rootScope.categories )
	 
		$scope.SupplierName = function(SupplierIndex)
		{
			var sName = DealsFactory.getSupplierNameById(SupplierIndex);
			return sName;
		}
		
		$rootScope.$watch('deals', function()
		{
			$scope.Deals = $rootScope.deals;
			console.log("Data2 : " , $rootScope.categories )
		});
	});
})


.controller('DealCtrl', function($scope,DealsFactory,$rootScope,$stateParams,$timeout) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		$scope.Deal = $rootScope.deals[$stateParams.ItemId];
		$scope.Gallery = [$scope.Deal.image,$scope.Deal.image2];
		$scope.host = $rootScope.Host;
		$scope.dealsImageGallery = $scope.Deal.gallery;
		
		console.log($scope.dealsImageGallery)
		
		$scope.SupplierName = function(SupplierIndex)
		{
			var sName = DealsFactory.getSupplierNameById(SupplierIndex);
			return sName;
		}
		
	}); 
})

.controller('ProfileCtrl', function($scope,DealsFactory,$rootScope,$stateParams,$timeout) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		$scope.navTitle='<img class="title-image" src="img/head_logo.png" />'
		$scope.Deals = [];
		$scope.host = $rootScope.Host
		$scope.Deals = $rootScope.deals;
		console.log("Data2 : " , $rootScope.categories )
	 
		$scope.SupplierName = function(SupplierIndex)
		{
			var sName = DealsFactory.getSupplierNameById(SupplierIndex);
			return sName;
		}
		
		$rootScope.$watch('deals', function()
		{
			$scope.Deals = $rootScope.deals;
			console.log("Data2 : " , $rootScope.categories )
		});
	}); 
})


.controller('editDealCtrl', function($scope,$rootScope,$stateParams,DealsFactory,$timeout,$http,$cordovaCamera,$ionicPopup,$localStorage,dateFilter) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		 $timeout(function() 
		 {
			$scope.host = $rootScope.Host;
    		$scope.DealId = $stateParams.ItemId;
			
			$scope.Deal = 
			{
				"title" : "",
				"deal_date" : "",
				"old_price" : "",
				"price" : "",
				"desc" : "",
				"smallletters" : ""
			}

			if ($scope.DealId !="-1")
			{
				$scope.Deal = DealsFactory.getDealById($scope.DealId);
				//$scope.Deal.old_price = parseInt($scope.Deal.old_price);
				//$scope.Deal.price = parseInt($scope.Deal.price);
				$scope.DealImage = $rootScope.Host+$scope.Deal.image;
				console.log("getDealById",$scope.Deal);
				$scope.Url = 'update_deal.php';
			}
			else
			{
				$scope.DealImage = $rootScope.Host+$localStorage.galleries[0];
				$scope.Url = 'add_new_deal.php';
				
			}




		
		$scope.SaveDeal = function()
		{

		if ($localStorage.galleries[0])
		{
			$scope.newimage = $localStorage.galleries[0];
		}	
		else
		{
			$scope.newimage = $scope.Deal.image;
		}
		
		if ($scope.Deal.title =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין כותרת הקופון',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.Deal.deal_date =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין תוקף הקופון',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.Deal.old_price =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין מחיר לפני הנחה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.Deal.price =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין מחיר אחרי הנחה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.Deal.desc =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין תיאור הקופון',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}		
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			$scope.newDate = dateFilter($scope.Deal.deal_date, 'dd/MM/yyyy');
			
				deal_params = 
				{
					"id" : $scope.DealId,
					"user" : $localStorage.userid,
					"title" : $scope.Deal.title,
					"date" : $scope.newDate,
					"oldprice" : $scope.Deal.old_price,
					"price" : $scope.Deal.price,
					"desc" : $scope.Deal.desc,
					"smallletters" : $scope.Deal.smallletters,
					"image" : $scope.newimage,
					"images" : $localStorage.galleries,
					"send" : 1

				}

				
				//console.log(login_params)
				$http.post($rootScope.Host+'/'+$scope.Url,deal_params)
				.success(function(data, status, headers, config)
				{
				})
				.error(function(data, status, headers, config)
				{

				});	

			window.location.href = "#/app/profile";
			

		   
		}
		

		}



			
 		 }, 200);
	}); 
})

.controller('editProfileCtrl', function($scope,$rootScope,$stateParams,DealsFactory,$timeout) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		 $timeout(function() 
		 {
			$scope.host = $rootScope.Host;
 		 }, 200);
	}); 
})



.controller('dealGalleryCtrl', function($scope,$rootScope,$stateParams,DealsFactory,$timeout,$cordovaCamera,$ionicPopup,$localStorage) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		 $timeout(function() 
		 {
			$scope.host = $rootScope.Host;
			$scope.galleryImages = new Array();
			if ($localStorage.galleries)
			{
				$localStorage.galleries = '';
			}
			
			$scope.CameraOption = function() 
			{
				
				var myPopup = $ionicPopup.show({
				//template: '<input type="text" ng-model="data.myData">',
				//template: '<style>.popup { width:500px; }</style>',
				title: 'בחר/י אפשרות:',
				scope: $scope,
				cssClass: 'custom-popup',
				buttons: [


			   {
				text: 'מצלמה',
				type: 'button-positive',
				onTap: function(e) { 
				  $scope.takePicture(1);
				}
			   },
			   {
				text: 'אלבום תמונות',
				type: 'button-calm',
				onTap: function(e) { 
				 $scope.takePicture(0);
				}
			   },
				   {
				text: 'ביטול',
				type: 'button-assertive',
				onTap: function(e) {  
				  //alert (1)
				}
			   },
			   ]
			  });
			};

			// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
			$scope.takePicture = function(index) {
				 var options ;
				 
				
				if(index == 1 )
				{
					options = { 
						quality : 75, 
						destinationType : Camera.DestinationType.FILE_URI, 
						sourceType : Camera.PictureSourceType.CAMERA, 
						allowEdit : true,
						encodingType: Camera.EncodingType.JPEG,
						targetWidth: 600,
						targetHeight: 400,
						popoverOptions: CameraPopoverOptions,
						saveToPhotoAlbum: false,
						correctOrientation: true
					};
				}
				else
				{
					options = { 
						quality : 75, 
						destinationType : Camera.DestinationType.FILE_URI, 
						sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
						allowEdit : true,
						encodingType: Camera.EncodingType.JPEG,
						targetWidth: 600,
						targetHeight: 600,
						popoverOptions: CameraPopoverOptions,
						saveToPhotoAlbum: false,
						correctOrientation : true
					};
				}
			 
				$cordovaCamera.getPicture(options).then(function(imageData) 
				{
					$scope.imgURI = imageData
					var myImg = $scope.imgURI;
					var options = new FileUploadOptions();
					options.mimeType = 'jpeg';
					options.fileKey = "file";
					options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
					//alert(options.fileName)
					var params = {};
					//params.user_token = localStorage.getItem('auth_token');
					//params.user_email = localStorage.getItem('email');
					options.params = params;
					var ft = new FileTransfer();
					ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
				});
				
				$scope.onUploadSuccess = function(data)
				{
					
					$timeout(function() 
					{ 
						if (data.response) {
							$scope.image = data.response;
							$scope.galleryImages.push($scope.image);
							//$scope.image.submit = false;
							$scope.newimage = $rootScope.Host+data.response;
						}
					}, 1000);

				}
				
				$scope.onUploadFail = function(data)
				{
					$scope.image.submit = false;
					//alert("onUploadFail : " + data)
				}
				
			}			
			
			$scope.saveGallery = function()
			{
				$localStorage.galleries = $scope.galleryImages;
				window.history.back();
			}

 		 }, 200);
	}); 
})

.controller('registerCtrl', function($scope,DealsFactory,$rootScope,$localStorage,$http,$ionicPopup) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		
		$scope.register = 
		{
			"name" : "",
			"email" : "",
			"phone" : "",
			"password" : ""
		}
		
		
		$scope.registerBtn = function()
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
				send_params = 
				{
					"name" : $scope.register.name,
					"email" : $scope.register.email,
					"phone" : $scope.register.phone,
					"password" : $scope.register.password,
					"send" : 1

				}

				
				//console.log(login_params)
				$http.post($rootScope.Host+'/register.php',send_params)
				.success(function(data, status, headers, config)
				{
					if (data.response.login == 1)
					{
						$ionicPopup.alert({
						 title: 'אימייל כבר קיים במערכת יש לבחור אימייל אחר',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });	

						$scope.register.email = '';
					}
					else
					{
						$localStorage.userid = data.response.userid;
						$localStorage.name = $scope.register.name;
						$localStorage.email = $scope.register.email;
						$localStorage.phone =  $scope.register.phone;	

						$scope.register.name = '';
						$scope.register.email = '';
						$scope.register.phone = '';
						$scope.register.password = '';
						
						window.location.href = "#/app/main";						
					}

				})
				.error(function(data, status, headers, config)
				{

				});	

				
		}
	});
})


.controller('loginCtrl', function($scope,DealsFactory,$rootScope,$localStorage,$http,$ionicPopup) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		
		$scope.login = 
		{
			"email" : "",
			"password" : ""
		}
		
		
		$scope.loginBtn = function()
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
				send_params = 
				{
					"email" : $scope.login.email,
					"password" : $scope.login.password,
					"send" : 1

				}

				
				//console.log(login_params)
				$http.post($rootScope.Host+'/login.php',send_params)
				.success(function(data, status, headers, config)
				{
					if (data.response.login == 1)
					{
						$ionicPopup.alert({
						 title: 'אימייל או סיסמה שגוים יש לנסות שוב',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });	

						
					}
					else
					{
						$localStorage.userid = data.response.userid;
						$localStorage.name = data.response.name;
						$localStorage.email = data.response.email;
						$localStorage.phone =  data.response.phone;

						$scope.login.email = '';
						$scope.login.password = '';
						
						window.location.href = "#/app/main";						
					}

				})
				.error(function(data, status, headers, config)
				{

				});	

				
		}
	});
})


.filter('toTrusted', function ($sce) 
{
    return function (value) {
        return $sce.trustAsHtml(value);
    };
});



